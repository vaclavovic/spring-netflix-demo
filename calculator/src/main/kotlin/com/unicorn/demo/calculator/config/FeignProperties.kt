package com.unicorn.demo.calculator.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix="feign")
data class FeignProperties(
  var maxAttempts : Int = 1,
  var period : Long = 1,
  var duration: Long = 1
)
