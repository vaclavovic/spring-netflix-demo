package com.unicorn.demo.calculator

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand
import com.unicorn.demo.calculator.client.DivisionClient
import com.unicorn.demo.calculator.client.MultiplicationClient
import com.unicorn.demo.calculator.model.CalculatorResult
import com.unicorn.demo.calculator.model.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CalculatorService {

    @Autowired
    lateinit var multiplicationClient : MultiplicationClient

    @Autowired
    lateinit var divisionClient : DivisionClient

    @HystrixCommand(fallbackMethod = "defaultResult")
    fun calculate(operation: Operation, x : Long, y : Long) : CalculatorResult {
      return CalculatorResult(operation, x, y,
              when (operation) {
                  Operation.MULTIPLY -> multiplicationClient.multiply(x, y).result
                  Operation.DIVIDE -> divisionClient.divide(x, y).result
              })
    }

    private fun defaultResult(operation: Operation, x : Long, y : Long) = CalculatorResult(operation, x, y, -1L)
}
