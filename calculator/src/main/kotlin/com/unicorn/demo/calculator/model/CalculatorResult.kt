package com.unicorn.demo.calculator.model

data class CalculatorResult(
        var operation: Operation?,
        var x : Long,
        var y : Long,
        var result : Long
)
