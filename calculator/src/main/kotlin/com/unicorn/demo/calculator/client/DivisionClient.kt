package com.unicorn.demo.calculator.client

import com.unicorn.demo.calculator.model.CalculatorResult
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@FeignClient("division-service")
@RequestMapping("/api/v1")
interface DivisionClient {
    @RequestMapping(method = [RequestMethod.GET], path = ["/divide/{x}/{y}"], consumes = ["application/json"])
    fun divide(@PathVariable("x") x: Long?, @PathVariable("y") y: Long?): CalculatorResult
}
