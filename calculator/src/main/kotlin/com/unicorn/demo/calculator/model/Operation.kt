package com.unicorn.demo.calculator.model

enum class Operation {
  DIVIDE,
  MULTIPLY
}
