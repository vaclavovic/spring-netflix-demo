package com.unicorn.demo.calculator

import com.unicorn.demo.calculator.model.CalculatorResult
import com.unicorn.demo.calculator.model.Operation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class CalculatorController {

  @Autowired
  lateinit var calculatorService : CalculatorService

  @GetMapping("/api/v1/calculate/{x}/{operation}/{y}")
  fun multiply(
          @PathVariable("operation") operation: Operation,
          @PathVariable("x") x : Long,
          @PathVariable("y") y: Long)

          : CalculatorResult = calculatorService.calculate(operation, x, y)
}
