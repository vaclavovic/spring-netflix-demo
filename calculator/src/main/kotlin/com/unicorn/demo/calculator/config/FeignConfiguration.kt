package com.unicorn.demo.calculator.config

import feign.Response
import feign.RetryableException
import feign.Retryer
import feign.codec.ErrorDecoder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import java.util.concurrent.TimeUnit

@Configuration
@EnableConfigurationProperties(FeignProperties::class)
class FeignConfiguration {

    @Autowired
    lateinit var feignProperties : FeignProperties

    @Bean
    fun retryer() : Retryer
            = Retryer.Default(feignProperties.period, TimeUnit.SECONDS.toMillis(feignProperties.duration), feignProperties.maxAttempts)

    @Bean
    fun errorDecoder() : ErrorDecoder = object : ErrorDecoder.Default() {
        override fun decode(methodKey : String, response : Response) : Exception {
            return if (HttpStatus.INTERNAL_SERVER_ERROR.value() == response.status()) {
                RetryableException("Ignore 500 error and retry!", null)
            } else {
                super.decode(methodKey, response)
            }
        }
    }
}


