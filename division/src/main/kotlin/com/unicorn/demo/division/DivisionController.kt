package com.unicorn.demo.division

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class DivisionController {

  @Autowired
  lateinit var divisionService : DivisionService

  @GetMapping("/api/v1/divide/{x}/{y}")
  fun multiply(@PathVariable("x") x : Long, @PathVariable("y") y: Long) = divisionService.divide(x, y)
}
