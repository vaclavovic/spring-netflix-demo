package com.unicorn.demo.division

data class DivisionResult(
  var x : Long,
  var y : Long,
  var result : Long
)
