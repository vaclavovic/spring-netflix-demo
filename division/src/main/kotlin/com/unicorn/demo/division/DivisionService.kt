package com.unicorn.demo.division

import org.springframework.stereotype.Service

@Service
class DivisionService {

  fun divide(x : Long, y : Long) : DivisionResult = DivisionResult(x, y, x / y)
}
