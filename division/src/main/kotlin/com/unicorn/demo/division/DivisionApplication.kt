package com.unicorn.demo.division

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class DivisionApplication

fun main(args: Array<String>) {
    runApplication<DivisionApplication>(*args)
}
