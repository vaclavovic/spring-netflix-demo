# README #

Demonstrace použití knihoven Spring Cloud Netflix.

### Co demo obsahuje? ###

* Discovery service (Eureka)
* Proxy service (Zuul)
* Client-side load balancer (Ribbon)
* Configuration service (Archaius)
* Declarative REST client (Feign)
* Circuit-breaker pattern (Hystrix)

### Programovací jazyk ###

Kotlin (https://kotlinlang.org)

