package com.unicorn.demo.multiplication

import org.springframework.stereotype.Service

@Service
class MultiplicationService {

  fun multiply(x : Long, y : Long) : MultiplicationResult {
    //throw RuntimeException("error")
    return MultiplicationResult(x, y, x * y)
  }
}
