package com.unicorn.demo.multiplication

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class MultiplicationController {

  @Autowired
  lateinit var multiplicationService : MultiplicationService

  @GetMapping("/api/v1/multiply/{x}/{y}")
  fun multiply(@PathVariable("x") x : Long, @PathVariable("y") y: Long) = multiplicationService.multiply(x, y)
}
