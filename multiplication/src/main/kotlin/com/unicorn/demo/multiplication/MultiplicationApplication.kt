package com.unicorn.demo.multiplication

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class MultiplicationApplication

fun main(args: Array<String>) {
    runApplication<MultiplicationApplication>(*args)
}
