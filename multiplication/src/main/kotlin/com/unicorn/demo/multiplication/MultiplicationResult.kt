package com.unicorn.demo.multiplication

data class MultiplicationResult(
  var x : Long,
  var y : Long,
  var result : Long
)
